document.addEventListener('DOMContentLoaded', function() {

  function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  let numberH = getRandomInRange(0, 5);
  if ($('.b-error__title').length) {
    $('.b-error__title span')[numberH].style.display='block';
  }


  /* ----------------------------------------------------------- */
  /* Проигрование видео */
  /* ----------------------------------------------------------- */
  let $buttonPlayVideo = $('.js-play-video');
  let $buttonScroll = $('.js-scroll-id');
  let $buttonCity = $('.js-hover-city');
  let $buttonDot = $('.map-dot');


  $buttonScroll.on('click', function() {
    $('html,body').animate({scrollTop: $(window).innerHeight() + 5}, 400);
  });

  $buttonPlayVideo.on('click', function() {
    let $video = $(this).next();
    $video.attr('controls', 'controls');
    $video[0].play();
    $(this).closest('.b-news-detail__video').addClass('hidden')
  });

  $buttonCity.hover(
    function() {
      let cityName = $(this).data('city');
      $(`.map-dot--${cityName}`).addClass('active')
    }, function() {
      let cityName = $(this).data('city');
      $(`.map-dot--${cityName}`).removeClass('active')
    });

  $buttonDot.hover(
    function() {
      let cityName = $(this).data('city');
      $(`.js-hover-city[data-city='${cityName}']`).closest('.b-map__item ').addClass('active')
    }, function() {
      let cityName = $(this).data('city');
      $(`.js-hover-city[data-city='${cityName}']`).closest('.b-map__item ').removeClass('active')
    });

  function initHegihtInContacts() {
    let numberTop = 150;
    let numberBottom = 100;
    let heightDesktopImg = $('.js-desktop-img img').innerHeight();

    if ($(window).innerWidth() <= 768) {
      numberTop = 298;
      numberBottom = 187;
      heightDesktopImg = $('.js-mobile-img img').innerHeight();
    }

    if ($(window).innerWidth() <= 540) {
      numberTop = 334;
      numberBottom = 190 - 60;
      heightDesktopImg = $('.js-mobile-img img').innerHeight();
    }

    let sum = heightDesktopImg + numberTop + numberBottom;
    $('.b-main-screen--contacts').css('height', `${sum}px`)
  }

  function initHegihtInCatalog() {
    let numberTop = 210;
    let numberBottom = 110;
    let heightDesktopImg = $('.b-catalog__desc-img-mobile img').innerHeight();

    if ($(window).innerWidth() <= 540) {
      numberTop = 168;
      numberBottom = 207 - 60;
    }

    let sum = heightDesktopImg + numberTop + numberBottom;
    $('.b-main-screen--catalog').css('height', `${sum}px`)
  }

  $(window).resize(function() {
    initHegihtInContacts();

    if ($(window).innerWidth() <= 768) {
      initHegihtInCatalog()
    }
  });


  setTimeout(function() {
    initHegihtInContacts();
  }, 350);
  

  if ($(window).innerWidth() <= 768) {
    setTimeout(function() {
      initHegihtInCatalog()
    }, 200)
  }

  $('.b-about-diag__icon[data-gr]').hover(
    function() {
      let number = $(this).data('gr');
      $(`.b-about-diag__gr[data-gr='${number}']`).addClass('active')
    }, function() {
      let number = $(this).data('gr');
      $(`.b-about-diag__gr[data-gr='${number}']`).removeClass('active')
    });


  function bodyFixPosition() {
    setTimeout(function() {
      if (!document.body.hasAttribute('data-body-scroll-fix')) {
        const scrollPosition = window.pageYOffset || document.documentElement.scrollTop

        document.body.style.width = `${$('body').innerWidth()}px`
        document.body.setAttribute('data-body-scroll-fix', scrollPosition)
        document.body.style.overflow = 'hidden'
        document.body.style.position = 'fixed'
        document.body.style.top = '-' + scrollPosition + 'px'
        document.body.style.left = '0'
      }
    }, 10)
  }

  function bodyUnfixPosition() {
    if (document.body.hasAttribute('data-body-scroll-fix')) {
      const scrollPosition = document.body.getAttribute('data-body-scroll-fix')

      document.body.style.width = ''
      document.body.removeAttribute('data-body-scroll-fix')
      document.body.style.overflow = ''
      document.body.style.position = ''
      document.body.style.top = ''
      document.body.style.left = ''

      window.scroll(0, scrollPosition)
    }
  }


  $('.js-open-menu').on('click', function() {
    $('.b-mobile-menu').addClass('b-mobile-menu--active').delay(280)
      .queue(function(nxt) {
        bodyFixPosition();
        $(this).addClass('b-mobile-menu--slide');
        nxt(); // continue the queue
      });
  });
  $('.js-close-menu').on('click', function() {
    $('.b-mobile-menu').removeClass('b-mobile-menu--slide').delay(280)
      .queue(function(nxt) {
        bodyUnfixPosition();
        $(this).removeClass('b-mobile-menu--active');
        nxt(); // continue the queue
      });
  });
  setTimeout(function() {
    $('.b-main-screen__sign').addClass('b-main-screen__sign--active')
  }, 630);

  var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
  if (isSafari) {
    $('.b-about-two-text__title').addClass('b-about-two-text__title--safari');
  }
});

